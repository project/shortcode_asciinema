# Shortcode Asciinema

This module provides an `[asciinema]` tag for the [shortcode](https://www.drupal.org/project/shortcode) module, allowing Drupal users to easily embed [Asciinema](https://asciinema.org/) videos without requiring the entry of a `<script>` tag into a text field.

Here is an example that uses all available attributes:

    [asciinema size="medium" autoplay="1" loop="1" theme="asciinema" speed="1" t="0"]105302[/asciinema]

The listed attribute values are all default values.
